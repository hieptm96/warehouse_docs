# API THUÊ KHO

## MỤC LỤC

 - [1. API THUÊ KHO](#1-api-thuê-kho)
 - [2. API LIST YÊU CẦU THUÊ KHO (THÔNG BÁO)](#2-api-list-yêu-cầu-thuê-kho)
 - [3. API PHÊ DUYỆT YÊU CẦU THUÊ KHO](#3-api-phê-duyệt-yêu-cầu-thuê-kho)
 - [4. API LỊCH SỬ THUÊ KHO](#4-api-lịch-sử-thuê-kho)

BASE URL: 

    
## 1. API API THUÊ KHO


> URL: 
 
 ```    
/api/rent/rent-a-warehouse
```    
    
> Method: 
```    
POST  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```    

POST BODY

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
warehouse_id | Require | integer | Kho
rent_area | Require | float | Diện tích
rent_time_from | Require | datetime | Thời gian thuê từ (Y-m-d H:i:s)
rent_time_to | Require | datetime | Thời gian thuê đến (Y-m-d H:i:s)

> response

```json
{
    "success": true,
    "message": "Đăng ký thuê kho thành công.",
    "data": {
        "rent_area": "20",
        "rent_time_from": "2019-07-24",
        "rent_time_to": "2019-07-29",
        "status": 0,
        "warehouse_id": 13,
        "user_id": 1,
        "updated_at": "2019-07-25 20:41:10",
        "created_at": "2019-07-25 20:41:10",
        "id": 1
    }
}
```

## 2. API LIST YÊU CẦU THUÊ KHO (THÔNG BÁO)


> URL: 
 
 ```    
/api/rent/get-pending-rent-warehouse
```    
    
> Method: 
```    
GET  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```   

PARAMS

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
status | Optional | integer | Trạng thái (Default không truyền là 0: chờ duyệt, 1 là đã duyệt, 2 là từ chối)

> response

```json
{
    "success": true,
    "message": "Success",
    "base_img": "http:\/\/localhost:8000\/storage\/images\/",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "rent_area": 20,
                "rent_time_from": "2019-07-28 00:00:00",
                "rent_time_to": "2019-08-28 00:00:00",
                "status": 0,
                "warehouse_id": 1,
                "user_id": 1,
                "deny_reason": null,
                "created_at": "2019-08-01 21:46:22",
                "updated_at": "2019-08-01 21:46:22",
                "title": "What is Lorem Ipsum?",
                "price": 20000000,
                "warehouse_image": "aaaa"
            }
        ],
        "first_page_url": "http:\/\/localhost:8000\/api\/rent\/get-pending-rent-warehouse?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http:\/\/localhost:8000\/api\/rent\/get-pending-rent-warehouse?page=1",
        "next_page_url": null,
        "path": "http:\/\/localhost:8000\/api\/rent\/get-pending-rent-warehouse",
        "per_page": 20,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```

## 3. API PHÊ DUYỆT YÊU CẦU THUÊ KHO


> URL: 
 
 ```    
/api/rent/update-rent-request
```    
    
> Method: 
```    
POST  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```    

POST BODY

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
id | Require | integer | Id yêu cầu
status | Require | integer | 1: đồng ý, 2: từ chối
deny_reason | Require | string | Nếu từ chối thì truyền lên lý do

> response

```json
{
    "success": true,
    "message": "Success"
}
```

## 4. API LỊCH SỬ THUÊ KHO


> URL: 
 
 ```    
/api/rent/get-rent-history
```    
    
> Method: 
```    
GET  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```   

PARAMS

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
sort_time | Optional | string | Sắp xếp theo desc (mới nhất), asc (cũ nhất)
status | Optional | integer | 0: chờ duyệt, 1: đã duyệt, 2: từ chối

> response

```json
{
    "success": true,
    "message": "Success",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "rent_area": 20,
                "rent_time_from": "2019-07-28 00:00:00",
                "rent_time_to": "2019-08-28 00:00:00",
                "status": 1,
                "warehouse_id": 1,
                "user_id": 1,
                "deny_reason": null,
                "created_at": "2019-08-01 21:46:22",
                "updated_at": "2019-08-01 21:46:22",
                "title": "What is Lorem Ipsum?",
                "price": 20000000,
                "rating": null,
                "total_rate": 0
            }
        ],
        "first_page_url": "http:\/\/localhost:8000\/api\/rent\/get-rent-history?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http:\/\/localhost:8000\/api\/rent\/get-rent-history?page=1",
        "next_page_url": null,
        "path": "http:\/\/localhost:8000\/api\/rent\/get-rent-history",
        "per_page": 20,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```