# API AUTHENTICATE

## MỤC LỤC

 - [1. API TẠO BÀI ĐĂNG KHO](#1-api-tạo-bài-đăng-kho)
 - [2. API LIST ĐỊA CHỈ](#2-api-list-địa-chỉ)
 - [3. API LIST BÀI ĐĂNG KHO](#3-api-list-bài-đăng-kho)
 - [4. API CHI TIẾT BÀI ĐĂNG KHO](#4-api-chi-tiết-bài-đăng-kho)
 - [5. API LIST BÀI ĐĂNG KHO CỦA NGƯỜI DÙNG](#5-api-list-bài-đăng-kho-của-người-dùng)
 - [6. API TẠO COMMENT](#6-api-tạo-comment)
 - [7. API LIST COMMENT CỦA 1 BÀI ĐĂNG](#7-api-list-comment-của-1-bài-đăng)
 - [8. API RATING](#8-api-rating)
 - [9. API DANH SÁCH CÁ NHÂN DOANH NGHIỆP THUÊ](#9-api-danh-sách-cá-nhân-doanh-nghiệp-thuê)

BASE URL: 

    
## 1. API TẠO BÀI ĐĂNG KHO


> URL: 
 
 ```    
/api/warehouse/new-warehouse
```    
    
> Method: 
```    
POST  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```    

POST BODY

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
owner | Require | integer | Đối tượng cho thuê (0: cá nhân, 1: doanh nghiệp)
warehouse_type | Require | integer | Loại kho (0: Quy chuẩn, 1: Không quy chuẩn)
area | Require | float | Diện tích
min_area | Require | float | Diện tích cho thuê tối thiểu
price | Require | integer | Giá thuê
rent_from | Require | datetime | Thời gian thuê từ (Y-m-d H:i:s)
rent_to | Require | datetime | Thời gian thuê đến (Y-m-d H:i:s)
province_id | Require | integer | Tỉnh/TP
district_id | Require | integer | Quận/Huyện
ward_id | Require | integer | Phường/Xã
first_address | Require | string | Số nhà/Ngõ (Địa chỉ chi tiết)
lat | Optional | string | lat (Google map)
lng | Optional | string | lng (Google map)
title | Require | string | Tiêu đề
description | Require | string | Mô tả kho
images[] | Optional | Array file | Hình ảnh kho (có thể gửi 1 hoặc nhiều file, gửi theo kiểu mảng)
video | Optional | String | Video
uptime_start | Optional | Time | Giờ bắt đầu hoạt động (H:i:s)
uptime_end | Optional | Time | Giờ kết thúc hoạt động (H:i:s)
floor | Optional | Integer | Tầng
total_floor_area | Optional | Float | Tổng diện tích mặt sàn
total_storage | Optional | Float | Tổng khối lượng lưu trữ
facade | Optional | Float | Mặt tiền
staircase | Optional | Bool | Thang máy
doors | Optional | Integer | Cửa bốc xếp hàng
max_weight_storage | Optional | Float | Trọng lượng lưu hàng tối đa
max_height_storage | Optional | Float | Chiều cao lưu hàng tối đa
direction | Optional | Integer | Hướng
construct_year | Optional | Integer | Năm xây dựng/làm mới
has_security | Optional | Bool | Bảo vệ 24/7
has_pccc | Optional | Bool | PCCC
has_machine | Optional | Bool | Dụng cụ & máy móc
has_equipment | Optional | Bool | Trang thiết bị
has_lau_goods | Optional | Bool | Bốc xếp hàng
has_order_process | Optional | Bool | Xử lý đơn hàng
has_transport | Optional | Bool | Vận chuyển


> response

```json
{
    "success": true,
    "message": "Đăng bài thành công"
}
```

## 2. API LIST ĐỊA CHỈ


> URL: 
 
 ```    
/api/list-addresses
```    
    
> Method: 
```    
GET  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```    

POST BODY

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
parent_id | Optional | integer | Không truyền default là lấy tỉnh

> response

```json
{
    "success": true,
    "message": "Lấy địa chỉ thành công",
    "data": [
        {
            "id": 1,
            "name": "Hà Nội",
            "parent_id": null
        },
        {
            "id": 126,
            "name": "TP Hồ Chí Minh",
            "parent_id": null
        },
        {
            "id": 129,
            "name": "Đà Nẵng",
            "parent_id": null
        },
    ]
}
```

## 3. API LIST BÀI ĐĂNG KHO


> URL: 
 
 ```    
/api/warehouse/list
```    
    
> Method: 
```    
GET  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```    

PARAMS

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
area_from | Optional | float | Diện tích >=
area_to | Optional | float | Diện tích <=
price_from | Optional | integer | Giá thuê >=
price_to | Optional | integer | Giá thuê <=
province_id | Optional | integer | Tỉnh/TP
district_id | Optional | integer | Quận/Huyện
ward_id | Optional | integer | Phường/Xã
warehouse_type | Optional | integer | Loại kho (0: Quy chuẩn, 1: Không quy chuẩn)
rent_from | Optional | integer | Thời gian thuê từ (Y-m-d H:i:s)
rent_to | Optional | integer | Thời gian thuê đến (Y-m-d H:i:s)
sort_created | Optional | string | Sắp xếp theo tin mới nhất (asc: tăng, desc: giảm)
sort_price | Optional | string | Sắp xếp theo giá (asc: tăng, desc: giảm)
sort_area | Optional | string | Sắp xếp theo diện tích (asc: tăng, desc: giảm)
sort_warehouse_type | Optional | string | Sắp xếp theo loại kho (asc: tăng, desc: giảm)
sort_owner | Optional | string | Sắp xếp theo doanh nghiệp/cá nhân thuê (asc: tăng, desc: giảm)

> response

```json
{
    "success": true,
    "message": "Success",
    "base_img": "http:\/\/localhost:8000\/storage\/images\/",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 13,
                "owner": 0,
                "warehouse_type": 0,
                "area": 120,
                "min_area": 20,
                "price": 20000000,
                "rent_from": "2019-07-24 00:00:00",
                "rent_to": "2019-07-30 00:00:00",
                "province_id": 1,
                "district_id": 2,
                "ward_id": 3,
                "first_address": 4,
                "lat": null,
                "lng": null,
                "video": "1\/b0a8f11c5b673c57c8fce84f1f317fb2_samplevideo-1280x720-1mbmp4.mp4",
                "title": "Cho thuê 1 nè",
                "description": "Mô tả nè",
                "is_public": 1,
                "user_id": 1,
                "deleted_at": null,
                "created_at": "2019-07-24 13:43:58",
                "updated_at": "2019-07-25 14:54:16",
                "images": [
                    {
                        "url": "1\/b0a8f11c5b673c57c8fce84f1f317fb2_gear-3s-200pxgif.gif",
                        "warehouse_id": 13
                    },
                    {
                        "url": "1\/b0a8f11c5b673c57c8fce84f1f317fb2_ghtk-dich-vu-giao-hang-trong-ngay-chuyen-nghieppng.png",
                        "warehouse_id": 13
                    }
                ]
            }
        ],
        "first_page_url": "http:\/\/localhost:8000\/api\/warehouse\/list?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http:\/\/localhost:8000\/api\/warehouse\/list?page=1",
        "next_page_url": null,
        "path": "http:\/\/localhost:8000\/api\/warehouse\/list",
        "per_page": 20,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```

## 4. API CHI TIẾT BÀI ĐĂNG KHO


> URL: 
 
 ```    
/api/warehouse/detail/{warehouse_id}
```    
    
> Method: 
```    
GET  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```    

> response

```json
{
    "success": true,
    "message": "Success",
    "base_img": "http:\/\/localhost:8000\/storage\/images\/",
    "data": {
        "id": 13,
        "owner": 0,
        "warehouse_type": 0,
        "area": 120,
        "min_area": 20,
        "price": 20000000,
        "rent_from": "2019-07-24 00:00:00",
        "rent_to": "2019-07-30 00:00:00",
        "province_id": 1,
        "district_id": 2,
        "ward_id": 3,
        "first_address": 4,
        "lat": null,
        "lng": null,
        "video": "1\/b0a8f11c5b673c57c8fce84f1f317fb2_samplevideo-1280x720-1mbmp4.mp4",
        "title": "Cho thuê 1 nè",
        "description": "Mô tả nè",
        "is_public": 1,
        "user_id": 1,
        "deleted_at": null,
        "created_at": "2019-07-24 13:43:58",
        "updated_at": "2019-07-25 14:54:16",
        "images": [
            {
                "url": "1\/b0a8f11c5b673c57c8fce84f1f317fb2_gear-3s-200pxgif.gif",
                "warehouse_id": 13
            },
            {
                "url": "1\/b0a8f11c5b673c57c8fce84f1f317fb2_ghtk-dich-vu-giao-hang-trong-ngay-chuyen-nghieppng.png",
                "warehouse_id": 13
            }
        ]
    },
    "comments": [
        {
            "id": 3,
            "user_id": 1,
            "content": "aaa",
            "warehouse_id": 2,
            "parent_id": 0,
            "created_at": "2019-07-29 22:39:06",
            "updated_at": "2019-07-29 22:39:06",
            "childrens": [
                {
                    "id": 4,
                    "user_id": 1,
                    "content": "aaa",
                    "warehouse_id": 2,
                    "parent_id": 3,
                    "created_at": "2019-07-29 22:39:12",
                    "updated_at": "2019-07-29 22:39:12"
                }
            ]
        }
    ]
}
```

## 5. API LIST BÀI ĐĂNG KHO CỦA NGƯỜI DÙNG


> URL: 
 
 ```    
/api/warehouse/list-by-current-user
```    
    
> Method: 
```    
GET  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```   

PARAMS

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
is_public | Optional | integer | Trạng thái(0: Chờ duyệt, 1: Đã duyệt, 2: Từ chối). Không truyền mặc định là tất cả

> response

```json
{
    "success": true,
    "message": "Success",
    "base_img": "http:\/\/localhost:8000\/storage\/images\/",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 13,
                "owner": 0,
                "warehouse_type": 0,
                "area": 120,
                "min_area": 20,
                "price": 20000000,
                "rent_from": "2019-07-24 00:00:00",
                "rent_to": "2019-07-30 00:00:00",
                "province_id": 1,
                "district_id": 2,
                "ward_id": 3,
                "first_address": 4,
                "lat": null,
                "lng": null,
                "video": "1\/b0a8f11c5b673c57c8fce84f1f317fb2_samplevideo-1280x720-1mbmp4.mp4",
                "title": "Cho thuê 1 nè",
                "description": "Mô tả nè",
                "is_public": 1,
                "user_id": 1,
                "deleted_at": null,
                "created_at": "2019-07-24 13:43:58",
                "updated_at": "2019-07-25 14:54:16",
                "images": [
                    {
                        "url": "1\/b0a8f11c5b673c57c8fce84f1f317fb2_gear-3s-200pxgif.gif",
                        "warehouse_id": 13
                    },
                    {
                        "url": "1\/b0a8f11c5b673c57c8fce84f1f317fb2_ghtk-dich-vu-giao-hang-trong-ngay-chuyen-nghieppng.png",
                        "warehouse_id": 13
                    }
                ]
            }
        ],
        "first_page_url": "http:\/\/localhost:8000\/api\/warehouse\/list-by-current-user?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http:\/\/localhost:8000\/api\/warehouse\/list-by-current-user?page=1",
        "next_page_url": null,
        "path": "http:\/\/localhost:8000\/api\/warehouse\/list-by-current-user",
        "per_page": 20,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```

## 6. API TẠO COMMENT


> URL: 
 
 ```    
/api/warehouse/{warehouse_id}/create-comment
```    
    
> Method: 
```    
POST  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```    

POST BODY

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
content | Require | string | Nội dung comment
parent_id | Optional | integer | Id comment cha (nếu có)


> response

```json
{
    "success": true,
    "message": "Tạo mới bình luận thành công",
    "data": {
        "content": "aaa",
        "user_id": 1,
        "warehouse_id": "13",
        "parent_id": "1",
        "updated_at": "2019-07-29 22:05:20",
        "created_at": "2019-07-29 22:05:20",
        "id": 2
    }
}
```

## 7. API LIST COMMENT CỦA 1 BÀI ĐĂNG


> URL: 
 
 ```    
/api/warehouse/{warehouse_id}/list-comment
```    
    
> Method: 
```    
GET  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```    


> response

```json
{
    "success": true,
    "message": "Lấy danh sách bình luận thành công",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 3,
                "user_id": 1,
                "content": "aaa",
                "warehouse_id": 2,
                "parent_id": 0,
                "created_at": "2019-07-29 22:39:06",
                "updated_at": "2019-07-29 22:39:06",
                "username": "taka2412",
                "childrens": [
                    {
                        "id": 4,
                        "user_id": 1,
                        "content": "aaa",
                        "warehouse_id": 2,
                        "parent_id": 3,
                        "created_at": "2019-07-29 22:39:12",
                        "updated_at": "2019-07-29 22:39:12",
                        "username": "taka2412"
                    }
                ]
            }
        ],
        "first_page_url": "http:\/\/localhost:8000\/api\/warehouse\/2\/list-comment?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http:\/\/localhost:8000\/api\/warehouse\/2\/list-comment?page=1",
        "next_page_url": null,
        "path": "http:\/\/localhost:8000\/api\/warehouse\/2\/list-comment",
        "per_page": 10,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```

## 8. API RATING


> URL: 
 
 ```    
/api/warehouse/{warehouse_id}/rating
```    
    
> Method: 
```    
POST  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```    

POST BODY

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
rate | Require | integer | Số sao rating


> response

```json
{
    "success": true,
    "message": "Success"
}
```

## 9. API DANH SÁCH CÁ NHÂN DOANH NGHIỆP THUÊ


> URL: 
 
 ```    
/api/warehouse/{warehouse_id}/get-list-rent
```    
    
> Method: 
```    
GET  
```  
    
> Header: 
```      
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0 
```   

PARAMS

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------

> response

```json
{
    "success": true,
    "message": "Success",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "rent_area": 20,
                "rent_time_from": "2019-07-24 00:00:00",
                "rent_time_to": "2019-07-29 00:00:00",
                "status": 1,
                "warehouse_id": 13,
                "user_id": 1,
                "created_at": "2019-07-25 20:41:10",
                "updated_at": "2019-07-25 20:41:10",
                "title": "Cho thuê 1 nè",
                "price": 20000000,
                "fullname": "Tô Mạnh Hiệp"
            }
        ],
        "first_page_url": "http:\/\/localhost:8000\/api\/warehouse\/13\/get-list-rent?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http:\/\/localhost:8000\/api\/warehouse\/13\/get-list-rent?page=1",
        "next_page_url": null,
        "path": "http:\/\/localhost:8000\/api\/warehouse\/13\/get-list-rent",
        "per_page": 20,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```
