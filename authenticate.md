# API AUTHENTICATE

## MỤC LỤC

 - [1. API ĐĂNG KÝ](#1-api-đăng-ký)
 - [2. API ĐĂNG NHẬP](#2-api-đăng-nhập)
 - [3. API LẤY THÔNG TIN USER](#3-api-lấy-thông-tin-user)
 - [4. API LOGOUT](#4-api-logout)

BASE URL: 

    
## 1. API ĐĂNG KÝ


> URL: 
 
 ```    
/api/auth/register
```    
    
> Method: 
```    
POST  
```  
    
> Header: 
```      
```    

POST BODY

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
fullname | Require | string | Họ và tên
tel | Require | string | Số điện thoại
address | Require | string | Địa chỉ
email | Require | string | Email
username | Require | string | Tên đăng nhập
password | Require | string | Mật khẩu
type | Optional | integer | 0 (Cá nhân), 1 (Doanh nghiệp) - Default 0


> response

```json
{
    "success": true,
    "message": "Register successfully.",
    "data": {
        "fullname": "Tô Mạnh Hiệp",
        "tel": "0969137941",
        "address": "Thái Bình",
        "email": "hiep1996tb@gmail.com",
        "username": "taka2412",
        "type": 0,
        "updated_at": "2019-07-23 03:10:08",
        "created_at": "2019-07-23 03:10:08",
        "id": 1
    }
}
```

## 2. API ĐĂNG NHẬP


> URL: 
 
 ```    
/api/auth/login
```    
    
> Method: 
```    
POST  
```  
    
> Header: 
```      
```    

POST BODY

Parameter | Mandatory | datatype | Description
--------- | ------- | ------- | -----------
email | Require | string | Email
password | Require | string | Mật khẩu


> response

```json
{
    "success": true,
    "message": "Login successfully",
    "data": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU2Mzk1MDA0OSwiZXhwIjoxNTYzOTUzNjQ5LCJuYmYiOjE1NjM5NTAwNDksImp0aSI6IldQaGVQT0VTYVFkVER6SWQiLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.hILHIe_DTPPdlkL-SiaJ-7p0HamjEtDJ8xrQapU-NwE",
        "user": {
            "id": 1,
            "username": "taka2412",
            "email": "hiep1996tb@gmail.com",
            "fullname": "Tô Mạnh Hiệp",
            "tel": "0969137941",
            "address": "Thái Bình",
            "type": 0,
            "email_verified_at": null,
            "created_at": "2019-07-23 03:10:08",
            "updated_at": "2019-07-23 03:10:08"
        }
    }
}
```

## 3. API LẤY THÔNG TIN USER


> URL: 
 
 ```    
/api/user-info
```    
    
> Method: 
```    
GET  
```  
    
> Header: 
```
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0      
```    

> response

```json
{
    "success": true,
    "message": "Lấy thông tin thành công",
    "data": {
        "id": 1,
        "username": "taka2412",
        "email": "hiep1996tb@gmail.com",
        "fullname": "Tô Mạnh Hiệp",
        "tel": "0969137941",
        "address": "Thái Bình",
        "type": 0,
        "email_verified_at": null,
        "created_at": "2019-07-23 03:10:08",
        "updated_at": "2019-07-23 03:10:08"
    }
}
```

## 4. API LOGOUT


> URL: 
 
 ```    
/api/auth/logout
```    
    
> Method: 
```    
POST  
```  
    
> Header: 
```
Authorization: Bearer fc98e8be71120f42cad70705927e4177c4b49bc5196281bfe820713b4718b2e0      
```    

> response

```json
{
    "success": true,
    "message": "Logout successfully"
}
```
